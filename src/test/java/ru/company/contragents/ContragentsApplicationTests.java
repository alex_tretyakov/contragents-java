package ru.company.contragents;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.company.dao.*;
import ru.company.entity.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration({
		"file:src/main/resources/META-INF/context.xml",
})
@SpringBootTest
public class ContragentsApplicationTests {

	@Autowired
	UserDao userDao;
	@Autowired
	ContragentDao contragentDao;
	@Autowired
	OrgTypeDao orgTypeDao;

	@Test
	public void contextLoads() {
		System.out.println("Test");
		List<User> users = userDao.getUsers();
		System.out.println("Count users = " + users.size());
		System.out.println(users);

		List<OrgType> orgTypes = orgTypeDao.getOrgTypes();
		System.out.println(orgTypes);

		Contragent contragent = createContragent();
		contragentDao.addContragent(contragent);

		System.out.println("количество контрагентов " + contragentDao.getContragents().size());
		System.out.println(contragentDao.getContragent(contragent.getId()));

		contragent.setName("new Name");
		contragentDao.updateContragent(contragent);

		System.out.println("количество контрагентов " + contragentDao.getContragents().size());
		System.out.println("имя - " + contragentDao.getContragents().get(0).getName());

		contragentDao.deleteContragent(contragent.getId());
		System.out.println("количество контрагентов " + contragentDao.getContragents().size());

	}

	private Contragent createContragent(){
		Contragent contragent = new Contragent();
		contragent.setName("contragentName");
		contragent.setFullName("full name");
		contragent.setOrgTypeId(1L);
		contragent.setInn("123");
		contragent.setPhone("987456");
		contragent.setEmail("email@email.email");
		contragent.setUser(2L);

		ArrayList<Address> addresses = new ArrayList<>();
		addresses.add(new Address(1L, "street1"));
		addresses.add(new Address(2L, "street2"));
		contragent.setAddresses(addresses);

		ArrayList<ContactFace> contactFaces = new ArrayList<>();
		contactFaces.add(new ContactFace("name", "123456", "email"));
		contragent.setContactFaces(contactFaces);

		return contragent;
	}
}
