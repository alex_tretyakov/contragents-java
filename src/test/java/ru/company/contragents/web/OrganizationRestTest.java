package ru.company.contragents.web;

import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.company.contragents.ContragentsApplication;
import ru.company.contragents.model.Organization;
import ru.company.contragents.service.OrganizationService;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContragentsApplication.class})
@WebAppConfiguration
public class OrganizationRestTest {

    private MockMvc mockMvc;

    @Mock
    private OrganizationService organizationServiceMock;

    @InjectMocks
    private OrganizationRest organizationRest;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(organizationRest)
                .build();
    }

    @Test
    public void getOrganizationTypes() throws Exception {
        Organization organization1 = new Organization("OOO");
        Organization organization2 = new Organization("IP");

        when(organizationServiceMock.getTypesOrganization()).thenReturn(Arrays.asList(organization1, organization2));

        mockMvc.perform(get("/organization-types").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json; charset=UTF-8"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].name", is("OOO")))
                .andExpect(jsonPath("$[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].name", is("IP")));

        verify(organizationServiceMock, times(1)).getTypesOrganization();
        verifyNoMoreInteractions(organizationServiceMock);
    }

    @Test
    public void getEmptyOrganizationTypes() throws Exception {
        when(organizationServiceMock.getTypesOrganization()).thenReturn(null);

        mockMvc.perform(get("/organization-types").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isNoContent());

        verify(organizationServiceMock, times(1)).getTypesOrganization();
        verifyNoMoreInteractions(organizationServiceMock);
    }
}
