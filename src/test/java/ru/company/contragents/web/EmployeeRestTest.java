package ru.company.contragents.web;

import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.company.contragents.ContragentsApplication;
import ru.company.contragents.model.Employee;
import ru.company.contragents.service.EmployeeService;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ContragentsApplication.class})
@WebAppConfiguration
public class EmployeeRestTest {

    private MockMvc mockMvc;

    @Mock
    private EmployeeService employeeServiceMock;

    @InjectMocks
    private EmployeeRest employeeRest;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(employeeRest)
                .build();
    }

    @Test
    public void getEmployees() throws Exception {
        Employee employee1 = new Employee("Ivan Ivanov", "8999100");
        Employee employee2 = new Employee("Sergey Sergeev", "8999100");

        when(employeeServiceMock.getEmployees()).thenReturn(Arrays.asList(employee1, employee2));

        mockMvc.perform(get("/users").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json; charset=UTF-8"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].fullName", is("Ivan Ivanov")))
                .andExpect(jsonPath("$[0].phone", is("8999100")))
                .andExpect(jsonPath("$[1].id").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].fullName", is("Sergey Sergeev")))
                .andExpect(jsonPath("$[1].phone", is("8999100")));

        verify(employeeServiceMock, times(1)).getEmployees();
        verifyNoMoreInteractions(employeeServiceMock);
    }

    @Test
    public void getEmptyEmployees() throws Exception {
        when(employeeServiceMock.getEmployees()).thenReturn(null);

        mockMvc.perform(get("/users").accept(MediaType.parseMediaType("application/json; charset=UTF-8")))
                .andExpect(status().isNoContent());

        verify(employeeServiceMock, times(1)).getEmployees();
        verifyNoMoreInteractions(employeeServiceMock);
    }
}
