package ru.company;

import org.springframework.context.support.GenericXmlApplicationContext;
import ru.company.dao.ContragentDao;
import ru.company.dao.UserDao;

public class Main {

    public static void main (String[] args) {
        GenericXmlApplicationContext applicationContext = new GenericXmlApplicationContext("META-INF/context.xml");

        ContragentDao dao = applicationContext.getBean(ContragentDao.class);

        System.out.println(dao != null);
    }
}
