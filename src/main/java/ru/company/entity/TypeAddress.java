package ru.company.entity;

import javax.persistence.*;

/**
 * Created by user on 04.08.2017.
 */
@Entity
@Table(name = "TYPE_ADDRESS")
@NamedQuery(name = "findAllTypeAddresses", query = "SELECT t FROM TypeAddress t")
public class TypeAddress {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;

    public TypeAddress() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TypeAddress{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
