package ru.company.entity;

import javax.persistence.*;

/**
 * Created by user on 04.08.2017.
 */
@Embeddable
@Table(name = "ADDRESS")
public class Address {
    @Column(name = "TYPE_ADDRESS")
    private Long typeAddressId;
    @Column(name = "ADDRESS")
    private String address;

    public Address() {
    }

    public Address(Long typeAddressId, String address) {
        this.typeAddressId = typeAddressId;
        this.address = address;
    }

    public Long getTypeAddress() {
        return typeAddressId;
    }

    public void setTypeAddress(Long typeAddress) {
        this.typeAddressId = typeAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "typeAddressId=" + typeAddressId +
                ", address='" + address + '\'' +
                '}';
    }
}
