package ru.company.entity;

import javax.persistence.*;

/**
 * Created by user on 04.08.2017.
 */
@Entity
@Table(name = "ORG_TYPE")
@NamedQuery(name = "findAllOrgTypes", query = "SELECT o FROM OrgType o")
public class OrgType {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;

    public OrgType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OrgType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}