package ru.company.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by user on 04.08.2017.
 */
@Entity
@Table(name = "CONTRAGENT")
@NamedQuery(name = "findAllContragents", query = "SELECT c FROM Contragent c")
public class Contragent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FULL_NAME")
    private String fullName;
    @Column(name = "NAME")
    private String name;
    @Column(name = "ORG_TYPE_ID")
    private Long orgTypeId;
    @Column(name = "INN")
    private String inn;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "USER_ID")
    private Long userId;
    //@Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "ADDRESS", joinColumns = @JoinColumn(name = "CONTRAGENT_ID"))
    private List<Address> addresses;
    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "CONTACT_FACE", joinColumns = @JoinColumn(name = "CONTRAGENT_ID"))
    private List<ContactFace> contactFaces;



    public Contragent() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(Long orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUser() {
        return userId;
    }

    public void setUser(Long user) {
        this.userId = user;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<ContactFace> getContactFaces() {
        return contactFaces;
    }

    public void setContactFaces(List<ContactFace> contactFaces) {
        this.contactFaces = contactFaces;
    }

    @Override
    public String toString() {
        return "Contragent{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", name='" + name + '\'' +
                ", orgTypeId=" + orgTypeId +
                ", inn='" + inn + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", userId=" + userId +
                ", addresses=" + addresses +
                ", contactFaces=" + contactFaces +
                '}';
    }
}
