package ru.company.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.company.entity.Contragent;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by user on 04.08.2017.
 */
@Repository
public class ContragentDaoImpl implements ContragentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void addContragent(Contragent contragent) {
        entityManager.persist(contragent);
    }

    @Override
    @Transactional
    public void updateContragent(Contragent contragent) {
        entityManager.merge(contragent);
    }

    @Override
    @Transactional
    public void deleteContragent(long id) {
        Contragent contragent = entityManager.find(Contragent.class, id);
        entityManager.remove(contragent);
    }

    @Override
    public Contragent getContragent(long id) {
        Contragent contragent = entityManager.find(Contragent.class, id);

        return contragent;
    }

    @Override
    public List<Contragent> getContragents() {
        List<Contragent> contragents = entityManager.createNamedQuery("findAllContragents", Contragent.class).getResultList();

        return contragents;
    }
}
