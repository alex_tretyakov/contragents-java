package ru.company.dao;

import ru.company.entity.OrgType;

import java.util.List;

/**
 * Created by user on 07.08.2017.
 */
public interface OrgTypeDao {
    List<OrgType> getOrgTypes();

    OrgType getOrgTypeById(Long id);
}
