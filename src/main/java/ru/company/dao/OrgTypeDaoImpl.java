package ru.company.dao;

import org.springframework.stereotype.Repository;
import ru.company.entity.OrgType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by user on 07.08.2017.
 */
@Repository
public class OrgTypeDaoImpl implements OrgTypeDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrgType> getOrgTypes() {
        List<OrgType> orgTypes = entityManager.createNamedQuery("findAllOrgTypes", OrgType.class).getResultList();

        return orgTypes;
    }

    @Override
    public OrgType getOrgTypeById(Long id) {
        return entityManager.find(OrgType.class, id);
    }


}
