package ru.company.dao;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

/**
 * Created by user on 08.08.2017.
 */
public class PopulatedDataSource implements FactoryBean<DataSource> {

    private DataSource dataSource;

    private String script;

    public DataSource getObject() throws Exception {
        if (script != null && !script.isEmpty()) {
            Resource initSchema = new ClassPathResource(script);
            DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema);
            DatabasePopulatorUtils.execute(databasePopulator, dataSource);
        }

        return dataSource;
    }

    public Class<?> getObjectType() {
        return DataSource.class;
    }

    public boolean isSingleton() {
        return true;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
