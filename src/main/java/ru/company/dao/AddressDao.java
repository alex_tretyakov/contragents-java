package ru.company.dao;

import ru.company.entity.Address;

/**
 * Created by user on 10.08.2017.
 */
public interface AddressDao {
    Address getAddressById(Long id);
}
