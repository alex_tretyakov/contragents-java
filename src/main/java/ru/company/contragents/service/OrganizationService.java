package ru.company.contragents.service;

import org.springframework.stereotype.Service;
import ru.company.contragents.model.Organization;

import java.util.List;

@Service
public interface OrganizationService {

    List<Organization> getTypesOrganization();

}
