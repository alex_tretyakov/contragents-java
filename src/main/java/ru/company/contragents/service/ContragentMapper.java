package ru.company.contragents.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.contragents.model.*;
import ru.company.dao.OrgTypeDao;
import ru.company.dao.TypeAddressDao;
import ru.company.dao.UserDao;
import ru.company.entity.ContactFace;
import ru.company.entity.OrgType;
import ru.company.entity.TypeAddress;
import ru.company.entity.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContragentMapper {
    @Autowired
    OrgTypeDao orgTypeDao;

    @Autowired
    UserDao userDao;

    @Autowired
    TypeAddressDao typeAddressDao;


    public ru.company.entity.Contragent mapContragentFromWebToDao(Contragent contragent) {
        if (contragent != null) {
            ru.company.entity.Contragent contragentDao = new ru.company.entity.Contragent();
            contragentDao.setId(contragent.getId());
            contragentDao.setName(contragent.getName());
            contragentDao.setFullName(contragent.getFullName());
            contragentDao.setOrgTypeId(contragent.getOrgType().getId());
            contragentDao.setInn(contragent.getFaxNumber());
            contragentDao.setPhone(contragent.getPhone());
            contragentDao.setEmail(contragent.getMail());
            contragentDao.setUser(mapEmployeeFromWebToDao(contragent.getResponsible()));
            contragentDao.setContactFaces(mapPersonsFromWebToDao(contragent.getContactPersons()));
            contragentDao.setAddresses(mapAddressesFromWebToDao(contragent.getAddresses()));
            return contragentDao;
        } else {
            return null;
        }
    }

    private Long mapEmployeeFromWebToDao(Employee employee) {
        if (employee != null) {
            User user = new User();
            user.setId(null);
            user.setName(employee.getFullName());
            user.setPhone(employee.getPhone());
            if (!checkIfUserExist(user)) {
                userDao.addUser(user);
                return getUserId(user);
            } else {
                return getUserId(user);
            }
        } else {
            return null;
        }
    }

    private Long getUserId(User user) {
        List<User> usersDao = userDao.getUsers();
        for (User currentUser : usersDao) {
            if (currentUser.equals(user)) {
                return currentUser.getId();
            }
        }
        return null;
    }

    private boolean checkIfUserExist(User user) {
        List<User> usersDao = userDao.getUsers();
        for (User currentUser : usersDao) {
            if (currentUser.equals(user)) {
                return true;
            }
        }
        return false;
    }

    private List<ru.company.entity.Address> mapAddressesFromWebToDao(List<Address> addresses) {
        List<ru.company.entity.Address> addressesDao = new ArrayList<>();

        for (Address addressWeb : addresses) {
            addressesDao.add(mapAddressFromWebToDao(addressWeb));
        }
        return addressesDao;
    }

    private ru.company.entity.Address mapAddressFromWebToDao(Address address) {
        if (address != null) {
            ru.company.entity.Address addressDao = new ru.company.entity.Address();
            List<TypeAddress> typeAddresses = typeAddressDao.getTypeAddresses();
            for (TypeAddress typeAddress : typeAddresses) {
                if (typeAddress.getId().equals(mapAddressTypeFromWebToDao(address.getAddressType()))) {
                    addressDao.setTypeAddress(typeAddress.getId());
                    addressDao.setAddress(address.getAddress());
                    return addressDao;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    private Long mapAddressTypeFromWebToDao(AddressType addressType) {
        if (addressType != null) {
            List<TypeAddress> typeAddresses = typeAddressDao.getTypeAddresses();
            for (TypeAddress typeAddress : typeAddresses) {
                if (typeAddress.getName().equals(addressType.getType()))
                    return typeAddress.getId();
            }
            return null;
        } else {
            return null;
        }
    }

    private static List<ContactFace> mapPersonsFromWebToDao(List<ContactPerson> contactPersons) {
        List<ContactFace> contactFaces = new ArrayList<>();

        for (ContactPerson contactPerson : contactPersons) {
            contactFaces.add(new ContactFace(contactPerson.getFullName(), contactPerson.getPhone(), contactPerson.getEmail()));
        }

        return contactFaces;
    }

    private static ContactFace mapPersonFromWebToDao(ContactPerson contactPerson) {
        if (contactPerson != null) {
            ContactFace contactFace = new ContactFace();

            contactFace.setName(contactPerson.getFullName());
            contactFace.setPhone(contactPerson.getPhone());
            contactFace.setEmail(contactPerson.getEmail());

            return contactFace;
        } else {
            return null;
        }
    }


    public List<Contragent> mapContragentsFromDaoToWeb(List<ru.company.entity.Contragent> contragentsDao) {
        List<Contragent> contragentsWeb = new ArrayList<>();

        for (ru.company.entity.Contragent contragentDao : contragentsDao) {
            contragentsWeb.add(mapContragentFromDaoToWeb(contragentDao));
        }

        return contragentsWeb;
    }

    public Contragent mapContragentFromDaoToWeb(ru.company.entity.Contragent contragentDao) {
        if (contragentDao != null) {
            Contragent contragentWeb = new Contragent();

            contragentWeb.setId(contragentDao.getId());
            contragentWeb.setName(contragentDao.getName());
            contragentWeb.setFullName(contragentDao.getFullName());
            contragentWeb.setOrgType(mapOrganizationFromDaoToWeb(contragentDao.getOrgTypeId()));
            contragentWeb.setFaxNumber(contragentDao.getInn());
            contragentWeb.setPhone(contragentDao.getPhone());
            contragentWeb.setMail(contragentDao.getEmail());
            contragentWeb.setResponsible(mapEmployeeFromDaoToWeb(contragentDao.getUser()));
            contragentWeb.setContactPersons(mapPersonsFromDaoToWeb(contragentDao));
            contragentWeb.setAddresses(mapAddressFromDaoToWeb(contragentDao));

            return contragentWeb;
        } else {
            return null;
        }
    }

    private AddressType mapAddressTypeFromDaoToWeb(Long addressTypeId) {
        if (addressTypeId != null) {
            List<TypeAddress> typeAddresses = typeAddressDao.getTypeAddresses();
            AddressType addressTypeWeb = new AddressType();
            for (TypeAddress typeAddress : typeAddresses) {
                if (typeAddress.getId().equals(addressTypeId)) {
                    addressTypeWeb.setId(addressTypeId);
                    addressTypeWeb.setType(typeAddress.getName());
                }
            }
            return addressTypeWeb;
        } else {
            return null;
        }
    }

    private List<Address> mapAddressFromDaoToWeb(ru.company.entity.Contragent contragentDao) {
        if (contragentDao != null) {
            List<Address> addressesWeb = new ArrayList<>();
            List<ru.company.entity.Address> addressesDao = contragentDao.getAddresses();

            for (ru.company.entity.Address addressDao : addressesDao) {
                addressesWeb.add(new Address(addressDao.getAddress(), mapAddressTypeFromDaoToWeb(addressDao.getTypeAddress())));
            }

            return addressesWeb;
        } else {
            return null;
        }
    }

    private static List<ContactPerson> mapPersonsFromDaoToWeb(ru.company.entity.Contragent contragentDao) {
        if (contragentDao != null) {
            List<ContactFace> contactFaces = contragentDao.getContactFaces();
            List<ContactPerson> contactPersonsWeb = new ArrayList<>();

            for (ContactFace contactFace : contactFaces) {
                contactPersonsWeb.add(mapPersonFromDaoToWeb(contactFace));
            }

            return contactPersonsWeb;
        } else {
            return null;
        }
    }

    // default type organization - ИП (id = 1)
    private Organization mapOrganizationFromDaoToWeb(Long orgTypeId) {
        Organization organizationWeb = new Organization();

        if (orgTypeId != null) {
            OrgType orgType = orgTypeDao.getOrgTypeById(orgTypeId);
            organizationWeb.setId(orgType.getId());
            organizationWeb.setName(orgType.getName());
        } else {
            organizationWeb = new Organization("ИП");
            organizationWeb.setId(1l);
        }
        return organizationWeb;
    }


    public List<Organization> mapOrganizationsFromDaoToWeb(List<OrgType> orgTypes) {
        List<Organization> organizations = new ArrayList<>();

        for (OrgType orgType : orgTypes) {
            organizations.add(mapOrganizationFromDaoToWeb(orgType.getId()));
        }

        return organizations;
    }


    private Employee mapEmployeeFromDaoToWeb(Long employeeId) {
        Employee employeeWeb = new Employee();

        if (employeeId != null) {
            User empDao = userDao.getUserById(employeeId);
            employeeWeb.setId(employeeId);
            employeeWeb.setFullName(empDao.getName());
            employeeWeb.setPhone(empDao.getPhone());
        } else {
            employeeWeb.setId(null);
            employeeWeb.setFullName(null);
            employeeWeb.setPhone(null);
        }
        return employeeWeb;
    }

    public List<Employee> mapEmployeesFromDaoToWeb(List<User> users) {
        List<Employee> employeesWeb = new ArrayList<>();

        for (User user : users) {
            employeesWeb.add(mapEmployeeFromDaoToWeb(user.getId()));
        }

        return employeesWeb;
    }

    private static ContactPerson mapPersonFromDaoToWeb(ContactFace personDao) {
        if (personDao != null) {
            ContactPerson contactPersonWeb = new ContactPerson();

            contactPersonWeb.setFullName(personDao.getName());
            contactPersonWeb.setEmail(personDao.getEmail());
            contactPersonWeb.setPhone(personDao.getPhone());

            return contactPersonWeb;
        } else {
            return null;
        }
    }

}
