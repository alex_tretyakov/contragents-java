package ru.company.contragents.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.contragents.model.Employee;
import ru.company.dao.UserDao;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    UserDao userDao;

    @Autowired
    ContragentMapper contragentMapper;

    @Override
    public List<Employee> getEmployees() {
        return contragentMapper.mapEmployeesFromDaoToWeb(userDao.getUsers());
    }
}
