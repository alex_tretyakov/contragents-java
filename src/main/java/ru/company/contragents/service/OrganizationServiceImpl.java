package ru.company.contragents.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.contragents.model.Organization;
import ru.company.dao.OrgTypeDao;

import java.util.List;

@Service
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
    OrgTypeDao orgTypeDao;

    @Autowired
    ContragentMapper contragentMapper;

    @Override
    public List<Organization> getTypesOrganization() {
        return  contragentMapper.mapOrganizationsFromDaoToWeb(orgTypeDao.getOrgTypes());
    }

}
