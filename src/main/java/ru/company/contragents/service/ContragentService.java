package ru.company.contragents.service;

import org.springframework.stereotype.Service;
import ru.company.contragents.model.Contragent;

import java.util.List;

@Service
public interface ContragentService {

    List<Contragent> getContragents();

    Contragent getContragentById(Long id);

    void updateContragent(Contragent contragent);

    void addContragent(Contragent contragent);

    void deleteContragent(Long id);

    boolean checkIfContragentExist(Long id);

    boolean checkIfContragentExist(Contragent contragent);

}