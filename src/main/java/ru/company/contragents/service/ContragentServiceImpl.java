package ru.company.contragents.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.contragents.model.Contragent;
import ru.company.dao.ContragentDao;

import java.util.List;

@Service
public class ContragentServiceImpl implements ContragentService {

    @Autowired
    ContragentDao contragentDao;


    @Autowired
    ContragentMapper contragentMapper;


    @Override
    public List<Contragent> getContragents() {
        return contragentMapper.mapContragentsFromDaoToWeb(contragentDao.getContragents());
    }

    @Override
    public Contragent getContragentById(Long id) {
        return contragentMapper.mapContragentFromDaoToWeb(contragentDao.getContragent(id));
    }

    @Override
    public void addContragent(Contragent contragent) {
        contragentDao.addContragent(contragentMapper.mapContragentFromWebToDao(contragent));
    }

    @Override
    public void updateContragent(Contragent contragent) {
        contragentDao.updateContragent(contragentMapper.mapContragentFromWebToDao(contragent));
    }

    @Override
    public void deleteContragent(Long id) {
        contragentDao.deleteContragent(id);
    }


    @Override
    public boolean checkIfContragentExist(Long id) {
        if (id == null) {
            return false;
        }
        else {
            return contragentDao.getContragent(id) != null;
        }
    }

    @Override
    public boolean checkIfContragentExist(Contragent contragent) {
        List<Contragent> contragentsDao = contragentMapper.mapContragentsFromDaoToWeb(contragentDao.getContragents());

        for (Contragent currentContragent : contragentsDao) {
            if (currentContragent.equals(contragent)) {
                return true;
            }
        }
        return false;
    }

}
