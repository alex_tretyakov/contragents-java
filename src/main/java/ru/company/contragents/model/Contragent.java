package ru.company.contragents.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Contragent {

    private Long id;

    @NotNull
    @Size(min=3, max=20)
    private String name;

    @NotNull
    @Size(min=10, max=30)
    private String fullName;

    private Organization orgType;

    @NotNull
    @Size(min=5, max=15)
    private String faxNumber;

    @NotNull
    @Size(min=5, max=20)
    private String phone;

    @NotNull
    @Size(min=5, max=30)
    private String mail;

    Employee responsible;

    List<ContactPerson> contactPersons;

    List<Address> addresses;

    public Contragent() {
    }

    public Contragent(String name, String fullName, Organization orgType, String faxNumber, String phone, String mail, Employee responsible, List<ContactPerson> contactPersons, List<Address> addresses) {
        this.name = name;
        this.fullName = fullName;
        this.orgType = orgType;
        this.faxNumber = faxNumber;
        this.phone = phone;
        this.mail = mail;
        this.responsible = responsible;
        this.contactPersons = contactPersons;
        this.addresses = addresses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Organization getOrgType() {
        return orgType;
    }

    public void setOrgType(Organization orgType) {
        this.orgType = orgType;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Employee getResponsible() {
        return responsible;
    }

    public void setResponsible(Employee responsible) {
        this.responsible = responsible;
    }

    public List<ContactPerson> getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(List<ContactPerson> contactPersons) {
        this.contactPersons = contactPersons;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contragent)) return false;

        Contragent that = (Contragent) o;

        if (!name.equals(that.name)) return false;
        return fullName.equals(that.fullName);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + fullName.hashCode();
        return result;
    }
}
