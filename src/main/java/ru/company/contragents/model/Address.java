package ru.company.contragents.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Address {

    private Long id;

    @NotNull
    @Size(min=5, max=50)
    private String address;

    private AddressType addressType;

    public Address() {
    }

    public Address(String address, AddressType addressType) {
        this.address = address;
        this.addressType = addressType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address1 = (Address) o;

        if (!address.equals(address1.address)) return false;
        return addressType.equals(address1.addressType);

    }

    @Override
    public int hashCode() {
        int result = address.hashCode();
        result = 31 * result + addressType.hashCode();
        return result;
    }
}
