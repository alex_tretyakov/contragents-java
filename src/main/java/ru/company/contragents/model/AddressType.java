package ru.company.contragents.model;

import javax.validation.constraints.Size;

public class AddressType {

    private Long id;

    @Size(min=5, max=20)
    private String type;

    public AddressType() {
    }

    public AddressType(String type) {

        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressType)) return false;

        AddressType that = (AddressType) o;

        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }
}
