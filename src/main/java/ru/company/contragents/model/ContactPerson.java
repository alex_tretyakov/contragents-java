package ru.company.contragents.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContactPerson {

    private Long id;

    @NotNull
    @Size(min=10, max=50)
    private String fullName;

    @NotNull
    @Size(min=5, max=20)
    private String phone;

    @NotNull
    @Size(min=5, max=30)
    private String email;

    public ContactPerson() {
    }

    public ContactPerson(String fullName, String phone, String email) {
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactPerson)) return false;

        ContactPerson that = (ContactPerson) o;

        if (!fullName.equals(that.fullName)) return false;
        if (!phone.equals(that.phone)) return false;
        return email.equals(that.email);

    }

    @Override
    public int hashCode() {
        int result = fullName.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
