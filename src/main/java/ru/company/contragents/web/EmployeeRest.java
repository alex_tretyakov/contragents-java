package ru.company.contragents.web;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.company.contragents.model.Employee;
import ru.company.contragents.service.EmployeeService;

import java.util.List;

@RestController
public class EmployeeRest {

    @Autowired
    EmployeeService employeeService;

    @ApiOperation(value = "View a list of employees", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of employees"),
            @ApiResponse(code = 204, message = "no employees at this moment")
    })
    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employees = employeeService.getEmployees();

        if (employees == null || employees.size() == 0) {
            return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
    }

}