package ru.company.contragents.web;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.company.contragents.model.Contragent;
import ru.company.contragents.model.Employee;
import ru.company.contragents.service.ContragentService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ContragentRest {

    @Autowired
    ContragentService contragentService;

    @ApiOperation(value = "View a list of contragents", response = Contragent.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of contragents"),
            @ApiResponse(code = 204, message = "there are not contragents at this moment")
    })
    @RequestMapping(value = "/contragents", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<Contragent>> getContragents() {
        List<Contragent> contragents = contragentService.getContragents();

        if (contragents == null || contragents.size() == 0) {
            return new ResponseEntity<List<Contragent>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Contragent>>(contragents, HttpStatus.OK);
    }

    @ApiOperation(value = "contragent with requested id", response = Employee.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved contragent"),
            @ApiResponse(code = 204, message = "no contragent with request id")
    })
    @RequestMapping(value = "/contragents/{contragentId}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<Contragent> getContragentById(@PathVariable Long contragentId) {
        Contragent contragent = contragentService.getContragentById(contragentId);

        if (contragent == null) {
            return new ResponseEntity<Contragent>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Contragent>(contragent, HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(code = 409, message = "contragent with request data already exists"),
            @ApiResponse(code = 201, message = "contragent added successfully")
    })
    @RequestMapping(value = "/contragents", method = RequestMethod.POST)
    public ResponseEntity<Void> addContragent(@Valid @RequestBody Contragent contragent, UriComponentsBuilder ucBuilder) {

        if (contragentService.checkIfContragentExist(contragent)) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        contragentService.addContragent(contragent);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/contragents/{id}").buildAndExpand(contragent.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "there are no contragents with request id"),
            @ApiResponse(code = 200, message = "contragent updated successfully")
    })
    @RequestMapping(value = "/contragents/{contragentId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateContragent(@PathVariable Long contragentId, @Valid @RequestBody Contragent contragent) {

        if (!contragentService.checkIfContragentExist(contragentId)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }

        contragent.setId(contragentId);
        contragentService.updateContragent(contragent);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "there are no contragents with request id"),
            @ApiResponse(code = 200, message = "contragent deleted successfully")
    })
    @RequestMapping(value = "/contragents/{contragentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteContragent(@PathVariable Long contragentId) {

        if (!contragentService.checkIfContragentExist(contragentId)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }

        contragentService.deleteContragent(contragentId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
